<?php defined ('BASEPATH') OR exit ('no direct script access allowed');

class jabatan_model extends CI_model
{
	//panggil nama table
	private $_table = "jabatan";
	
	public function tampilDataJabatan()
	{
		//seperti : select * from <nama_table> "cara 1"
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataJabatan2()
	{
		// CARA 2
		$query = $this->db->query("SELECT * FROM jabatan WHERE flag = 1");
		return $query->result();
	}
	public function tampilDataJabatan3()
	{
		// CARA 3
		$this->db->select('*');
		$this->db->order_by('kode_jabatan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}


	public function save()
	{
		
		
		$data['kode_jabatan']	= $this->input->post('kode_jabatan');
		$data['nama_jabatan']	= $this->input->post('jabatan');
		$data['keterangan']	= $this->input->post('Keterangan');
		
		
		$data['flag']	= 1;
		$this->db->insert($this->_table, $data);
	}
	
	
	public function detail($kode_jabatan)
	{
		$this->db->select('*');
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}

	public function update($kode_jabatan)
	{
		$data['nama_jabatan']	= $this->input->post('nama_jabatan');
		$data['keterangan']	= $this->input->post('Keterangan');
		$data['flag']	= 1;
		
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->update($this->_table, $data);
	}


	public function delete($kode_jabatan)
	{
		$this->db->where('kode_jabatan', $kode_jabatan);
		$this->db->delete($this->_table);
		
	}







}


