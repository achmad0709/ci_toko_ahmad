<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Supplier extends CI_controller {

	
	
	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");
		
	}
	
	public function index()
	
	{
		$this->listsupplier();
	}
	
	public function listsupplier()
	
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$this->load->view('HomeSupplier', $data);
	}


	public function Input()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		
		if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->save();
			redirect("supplier/index", "refresh"); 
		}
		
		
		$this->load->view('InputSupplier', $data);
	}
	
	   public function detailsupplier($kode_supplier)
	   {
			$data['detail_supplier']= $this->supplier_model->detailsupplier($kode_supplier);
			$this->load->view('detail_supplier', $data);   
	   }
	   public function Editsupplier($kode_supplier)
	{
	
		$data['detail_supplier'] = $this->supplier_model->detailsupplier($kode_supplier);
		
		if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->update($kode_supplier);
			redirect("supplier/index", "refresh"); 
		}
		
		$this->load->view('Editsupplier', $data);
	}
	
	public function deletesupplier($kode_supplier)
	{
		$m_supplier = $this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index", "refresh");
		
	}
	
	
	
	
	
	
}
