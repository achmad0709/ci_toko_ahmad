<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jenis_barang extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("jenis_barang_model");
	}
	
	public function index()
	
	{
		$this->listjenisBarang();
	}
	
	public function listjenisBarang()
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$this->load->view('HomeJenisBarang', $data);
	}


	public function input_brg()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		
		if (!empty($_REQUEST)) {
			$m_jenisbarang = $this->jenis_barang_model;
			$m_jenisbarang->save();
			redirect("Jenis_barang/index", "refresh"); 
		}
		
		
		$this->load->view('InputJenisBarang', $data);
	}
	
	   public function detail_jenis_barang($kode_jenis)
	   {
			$data['detail_jenis_barang']	= $this->jenis_barang_model->detail($kode_jenis);
			$this->load->view('detail_jenis_barang', $data);   
	   }

		public function Editjenisbarang($kode_jenis)
	{
	
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		
		if (!empty($_REQUEST)) {
			$m_jenisbarang = $this->jenis_barang_model;
			$m_jenisbarang->update($kode_jenis);
			redirect("Jenis_barang/index", "refresh"); 
		}
		
		
		$this->load->view('Editjenisbarang', $data);

	}

	public function deletejenisbarang($kode_jenis)
	{
		$m_jenisbarang = $this->jenis_barang_model;
		$m_jenisbarang->delete($kode_jenis);
		redirect("Jenis_barang/index", "refresh");
		
	}
	
	



}
